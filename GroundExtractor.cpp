//
//  GroundExtractor.cpp
//  PCLApp
//
//  Created by Jaromír Landa on 11/11/2020.
//

#include "GroundExtractor.hpp"

GroundExtractor::GroundExtractor(){
    
}

void GroundExtractor::extractGround(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud){
    // init variables for extracted ground and non-ground points
    pcl::PointCloud<pcl::PointXYZ>::Ptr groundPoints (new pcl::PointCloud<pcl::PointXYZ>);
    pcl::PointCloud<pcl::PointXYZ>::Ptr otherPoints (new pcl::PointCloud<pcl::PointXYZ>);

    pcl::PointIndicesPtr groundIndices(new pcl::PointIndices);

    // Filter for segmentation of ground points
    pcl::ProgressiveMorphologicalFilter<pcl::PointXYZ> filter;
    filter.setInputCloud(inputCloud);
    filter.setMaxWindowSize(20);
    filter.setSlope(1.0f);
    filter.setInitialDistance(0.5f);
    // max height above the ground surface
    filter.setMaxDistance(3.0f);
    // launch the segmentation algorithm and returns indices of points determined to be ground returns.
    cout << "Performing segmentation to determine point cloud's ground indices..." << endl;
    filter.extract(groundIndices->indices);
    cout << "Segmentation finished. Extracted ground indices successfully." << endl;
    
    pcl::ExtractIndices<pcl::PointXYZ> extractor;
    extractor.setInputCloud(inputCloud);
    // extract ground points
    extractor.setIndices(groundIndices);
    extractor.filter(*groundPoints);
    // extract non-ground points
    extractor.setNegative(true);
    extractor.filter(*otherPoints);
    // store ground and non-ground points
    ground = groundPoints;
    everyrhingElse = otherPoints;
    cout << "Extracted ground and non-ground points." << endl;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr GroundExtractor::getGround(){
    return ground;
}

pcl::PointCloud<pcl::PointXYZ>::Ptr GroundExtractor::getEverythingElse(){
    return everyrhingElse;
}
