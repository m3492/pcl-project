//
// Created by hladk on 31.10.2022.
//

#include "Segmentation.h"

std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> Segmentation::euclideanClusterSegmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloudToSegment){

    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> segmentedClouds;

    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree(new pcl::search::KdTree<pcl::PointXYZ>);
    tree->setInputCloud(cloudToSegment);

    std::vector<pcl::PointIndices> clusterIndices;
    pcl::EuclideanClusterExtraction<pcl::PointXYZ> euclideanClusterExtraction;
    euclideanClusterExtraction.setClusterTolerance(1.0);
    euclideanClusterExtraction.setMinClusterSize(60);
    euclideanClusterExtraction.setMaxClusterSize(25000);
    euclideanClusterExtraction.setSearchMethod(tree);
    euclideanClusterExtraction.setInputCloud(cloudToSegment);
    euclideanClusterExtraction.extract(clusterIndices);

    for (pcl::PointIndices clusterPointIndices : clusterIndices) {
        // init new cloud cluster
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudCluster(new pcl::PointCloud<pcl::PointXYZ>);
        for (int pointIdx : clusterPointIndices.indices) {
            cloudCluster->points.push_back(cloudToSegment->points[pointIdx]);
        }
        cloudCluster->width = cloudCluster->points.size();
        cloudCluster->height = 1;
        cloudCluster->is_dense = true;
        segmentedClouds.push_back(cloudCluster);
    }

    return segmentedClouds;
}

std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> Segmentation::getRoofBase(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cloudSegments){

    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> roofBaseClouds;

    for(pcl::PointCloud<pcl::PointXYZ>::Ptr segment : cloudSegments){

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudHull(new pcl::PointCloud<pcl::PointXYZ>);
        // create ConcaveHull by cloud segment
        pcl::ConcaveHull<pcl::PointXYZ> concaveHull;
        concaveHull.setInputCloud(segment);
        concaveHull.setAlpha(5);
        concaveHull.setDimension(2);
        // compute concave hull for all given points
        concaveHull.reconstruct(*cloudHull);

        // get MAX and MIN point of the roof
        pcl::PointXYZ minPoint, maxPoint;
        pcl::getMinMax3D(*segment, minPoint, maxPoint);
        std::cout << "Roof MAX point: x=" << maxPoint.x << ",y=" << maxPoint.y << ",z=" << maxPoint.z << endl;
        std::cout << "Roof MIN point: x=" << minPoint.x << ",y=" << minPoint.y << ",z=" << minPoint.z << endl;

        // get points of the top roof edges using the highest point with defined Z axis divergence
        float zDivergence = 1.2;
        std::vector<pcl::PointXYZ> topRoofEdge;
        for (int i = 0; i < segment->points.size(); i++){
            if (segment->points.at(i).z > (maxPoint.z - zDivergence)) {
                topRoofEdge.push_back(segment->points.at(i));
            }
        }
        // find the most distant points of the top roof edge
        pcl::PointXYZ topRoofPointA, topRoofPointB;
        float maxDistance = -1000.0;
        for (int i = 0; i < topRoofEdge.size(); i++){
            for (int j = 0; j < topRoofEdge.size(); j++){
                float distance = pcl::squaredEuclideanDistance(topRoofEdge.at(i), topRoofEdge.at(j));
                if (distance > maxDistance) {
                    maxDistance = distance;
                    topRoofPointA = topRoofEdge.at(i);
                    topRoofPointB = topRoofEdge.at(j);
                }
            }
        }
        // store the most distant points of the top roof edge
        cloudHull->points.push_back(topRoofPointA);
        cloudHull->points.push_back(topRoofPointB);
        roofBaseClouds.push_back(cloudHull);
    }

    return roofBaseClouds;
}

std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> Segmentation::simplifyRoofBases(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> hulls){

    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> simplifiedHulls;

    for(int i = 0; i < hulls.size(); i++){

        pcl::PointXYZ secondHighestPoint = hulls.at(i)->points.back();
        hulls.at(i)->points.pop_back();
        pcl::PointXYZ highestPoint = hulls.at(i)->points.back();
        hulls.at(i)->points.pop_back();

        // convert PCL hull points to OpenCV points
        std::vector<cv::Point2f> points;
        for(int j = 0; j < hulls.at(i)->points.size(); j++){
            cv::Point2f point;
            point.x = hulls.at(i)->points.at(j).x;
            point.y = hulls.at(i)->points.at(j).y;
            points.push_back(point);
        }

        // approximate hulls using OpenCV
        cv::approxPolyDP(points, points, 2, true);

        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudHull (new pcl::PointCloud<pcl::PointXYZ>);
        // convert back to PCL cloud hull
        for(int j = 0; j < points.size(); j++){
            pcl::PointXYZ point3D;
            point3D.x = points.at(j).x;
            point3D.y = points.at(j).y;
            point3D.z = 590;
            cloudHull->points.push_back(point3D);
        }
        // last two points are the most distant points of the top roof edge
        cloudHull->points.push_back(secondHighestPoint);
        cloudHull->points.push_back(highestPoint);
        // add approximated (simplified) hull
        simplifiedHulls.push_back(cloudHull);
    }

    return simplifiedHulls;
}