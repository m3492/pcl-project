#include <pcl/point_types.h>
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/ply_io.h>


#include "PLYLoader.hpp"
#include "PCLVisualization.hpp"
#include "Color.hpp"
#include "GroundExtractor.hpp"
#include "CloudDownSampler.h"
#include "Segmentation.h"


int main() {


    // init PLY loader
    PLYLoader loader;
    bool wasGroundExtractionExecuted = true;
    if (!wasGroundExtractionExecuted) {
        // load original point cloud
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = loader.loadCloud(R"(C:\Projects\gitlab\mendelu\pcl-project\resources\urban_area_without_ground.ply)");
        // downSample original cloud
        CloudDownSampler cloudDownSampler;
        pcl::PointCloud<pcl::PointXYZ>::Ptr downSampledCloud = cloudDownSampler.downSampleCloud(cloud);
        // extract ground
        GroundExtractor groundExtractor;
        groundExtractor.extractGround(downSampledCloud);
        // save GroundExtractor resulted clouds to PLY files
        pcl::PLYWriter plyWriter;
        plyWriter.write(R"(C:\Projects\gitlab\mendelu\pcl-project\resources\urban_area_ground.ply)", *groundExtractor.getGround());
        plyWriter.write(R"(C:\Projects\gitlab\mendelu\pcl-project\resources\urban_area_without_ground.ply)", *groundExtractor.getEverythingElse());
    }

    pcl::PointCloud<pcl::PointXYZ>::Ptr withoutGroundCloud = loader.loadCloud(R"(C:\Projects\gitlab\mendelu\pcl-project\resources\urban_area_without_ground.ply)");
    pcl::PointCloud<pcl::PointXYZ>::Ptr groundCloud = loader.loadCloud(R"(C:\Projects\gitlab\mendelu\pcl-project\resources\urban_area_ground.ply)");

    // segmentation
    Segmentation segmentation;
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cloudSegments = segmentation
            .euclideanClusterSegmentation(withoutGroundCloud);
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> roofHulls = segmentation.getRoofBase(cloudSegments);
    roofHulls = segmentation.simplifyRoofBases(roofHulls);

    // init clouds visualization
    PCVisualization pclVisualization;
    pclVisualization.initializeVisualization();
    // add clouds to visualization
//    pclVisualization.addCloud(cloud, Color(0, 255, 0));
//    pclVisualization.addCloud(withoutGroundCloud, Color(0, 255, 0));
    pclVisualization.addSegmentedCloud(cloudSegments);
    pclVisualization.addHulls(roofHulls);
    cout << "Running visualization..." << endl;
    pclVisualization.runVisualization();
    return 0;
}
