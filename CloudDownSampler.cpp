//
// Created by hladk on 31.10.2022.
//

#include "CloudDownSampler.h"

pcl::PointCloud<pcl::PointXYZ>::Ptr CloudDownSampler::downSampleCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud) {
    // init new point cloud
    pcl::PointCloud<pcl::PointXYZ>::Ptr filteredCloud(new pcl::PointCloud<pcl::PointXYZ>);
    // create voxel grid filter
    // each voxel (3D Box) will be approximated by with their centroid
    pcl::VoxelGrid<pcl::PointXYZ> voxelGridFilter;
    voxelGridFilter.setInputCloud(pointCloud);
    // customize leaf size to customize filtering
    voxelGridFilter.setLeafSize(1.0f, 1.0f, 1.0f);
    // downsample original cloud and save result to filteredCloud
    voxelGridFilter.filter(*filteredCloud);
    unsigned int origCloudSize = pointCloud->points.size();
    unsigned int filteredCloudSize = filteredCloud->points.size();
    cout << "Downsampled cloud with size: " + to_string(origCloudSize)
    + " to cloud with size: " + to_string(filteredCloudSize) << endl;
    return filteredCloud;
}