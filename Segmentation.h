//
// Created by hladk on 31.10.2022.
//

#ifndef PCL_PROJECT_SEGMENTATION_H
#define PCL_PROJECT_SEGMENTATION_H

#define HAVE_QHULL

#include <iostream>
#include <vector>

#include <pcl/segmentation/region_growing.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/extract_clusters.h>
#include <pcl/pcl_config.h>
#include <pcl/surface/concave_hull.h>

#include <pcl/common/common.h>
#include <pcl/common/distances.h>

#include <opencv2/opencv.hpp>

class Segmentation {
public:
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> euclideanClusterSegmentation(pcl::PointCloud<pcl::PointXYZ>::Ptr cloudToSegment);
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> getRoofBase(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> segmentedCloud);
    std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> simplifyRoofBases(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> hulls);
};


#endif //PCL_PROJECT_SEGMENTATION_H
