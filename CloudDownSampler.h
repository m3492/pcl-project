//
// Created by hladk on 31.10.2022.
//

#ifndef PCL_PROJECT_CLOUDDOWNSAMPLER_H
#define PCL_PROJECT_CLOUDDOWNSAMPLER_H

#include <pcl/filters/voxel_grid.h>
#include <string>

using namespace std;

class CloudDownSampler {
public:
    /*
     *
     */
    pcl::PointCloud<pcl::PointXYZ>::Ptr downSampleCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr pointCloud);

};


#endif //PCL_PROJECT_CLOUDDOWNSAMPLER_H
