#include "PCLVisualization.hpp"


PCVisualization::PCVisualization()
{
    m_numberOfClouds = 0;
}

void PCVisualization::initializeVisualization(){
    
    // inicializace visuzalizeru
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
    // nasetuju ho
    m_viewer = viewer;
    // nastavim pozadi
    m_viewer->setBackgroundColor (0, 0, 0);

}


void PCVisualization::runVisualization(){
    
    m_viewer->initCameraParameters();
    m_viewer->resetCameraViewpoint("cloud0");
    
    while (!m_viewer->wasStopped ())
    {
        m_viewer->spinOnce(100);
        std::this_thread::sleep_for(std::chrono::microseconds(100000));
    }
    m_viewer->close();
    
}


void PCVisualization::addCloud(pcl::PointCloud<pcl::PointXYZ>::Ptr cloud, Color pointsColor){
    
    // potrebuju jmeno
    std::stringstream stream;
    stream << "cloud" << m_numberOfClouds;
    std::string cloudName =  stream.str();
    
    // pridam cloud
    m_viewer->addPointCloud<pcl::PointXYZ> (cloud, cloudName);
    // nasetuju vlastnosti
    m_viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, cloudName);
    // naseruju barvu
    m_viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, (double)pointsColor.R/255, (double)pointsColor.G/255, (double)pointsColor.B/255, cloudName);
    // inkrementuju pocet mracen
    m_numberOfClouds++;
}

void PCVisualization::addHulls(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> hulls){

    int hullId = 1;
    for (auto & hull : hulls){

        for (int i = 0; i < hull->points.size() - 3; i++) {
            std::string lineId = "hull_" + std::to_string(hullId) + "line" + std::to_string(i);
            m_viewer->addLine(hull->points.at(i), hull->points.at(i + 1), lineId);

            float distanceA = pcl::squaredEuclideanDistance(hull->points.at(i), hull->points.at(hull->points.size() - 2));
            float distanceB = pcl::squaredEuclideanDistance(hull->points.at(i), hull->points.at(hull->points.size() - 1));
            lineId += "base_roof";
            if (distanceA < distanceB) {
                m_viewer->addLine(hull->points.at(i), hull->points.at(hull->points.size() - 2), lineId);
            } else {
                m_viewer->addLine(hull->points.at(i), hull->points.at(hull->points.size() - 1), lineId);
            }
        }

        // connecting the top roof points
        m_viewer->addLine(hull->points.at(hull->points.size() - 2), hull->points.at(hull->points.size() - 1), "hull_" + std::to_string(hullId) + "_top_roof_edge");
        // closing the roof base
        m_viewer->addLine(hull->points.at(0), hull->points.at(hull->points.size() - 3), "hull_" + std::to_string(hullId) + "_line_enclosing_base");

        float distanceA = pcl::squaredEuclideanDistance(hull->points.at(hull->points.size() - 3), hull->points.at(hull->points.size() - 2));
        float distanceB = pcl::squaredEuclideanDistance(hull->points.at(hull->points.size() - 3), hull->points.at(hull->points.size() - 1));
        std::string lineId = "base_roof";
        if (distanceA < distanceB) {
            m_viewer->addLine(hull->points.at(hull->points.size() - 3), hull->points.at(hull->points.size() - 2), "hull_" + std::to_string(hullId) + "_last_bottom_top");
        } else {
            m_viewer->addLine(hull->points.at(hull->points.size() - 3), hull->points.at(hull->points.size() - 1), "hull_" + std::to_string(hullId) + "_last_bottom_top");
        }

        hullId++;
        addCloud(hull, Color(0, 255, 0));
    }
}

void PCVisualization::addSegmentedCloud(std::vector<pcl::PointCloud<pcl::PointXYZ>::Ptr> cloudSegments){
    for(auto & segment : cloudSegments){
        addCloud(segment, getRandomColor());
    }
}

Color PCVisualization::getRandomColor(){

    int r = rand() % (255-0) + 0;
    int g = rand() % (255-0) + 0;
    int b = rand() % (255-0) + 0;
    return Color(r,g,b);
}


