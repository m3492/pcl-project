//
//  GroundExtractor.hpp
//  PCLApp
//
//  Created by Jaromír Landa on 11/11/2020.
//

#ifndef GroundExtractor_hpp
#define GroundExtractor_hpp

#include <cstdio>

#include <pcl/point_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/segmentation/progressive_morphological_filter.h>

using namespace std;


class GroundExtractor{
public:
    GroundExtractor();
    void extractGround(pcl::PointCloud<pcl::PointXYZ>::Ptr inputCloud);
    pcl::PointCloud<pcl::PointXYZ>::Ptr getGround();
    pcl::PointCloud<pcl::PointXYZ>::Ptr getEverythingElse();
    
private:
    pcl::PointCloud<pcl::PointXYZ>::Ptr ground;
    pcl::PointCloud<pcl::PointXYZ>::Ptr everyrhingElse;
    
    
    
};

#endif /* GroundExtractor_hpp */
